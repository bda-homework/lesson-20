
public class Main {
    public static void main(String[] args) {
        Time time = new Time(23, 11, 48, 975);

        printTime(time);
    }

    public static void printTime(Time time) {
        Class<? extends Time> timeClass = time.getClass();
        for (java.lang.reflect.Field field : timeClass.getDeclaredFields()) {
            if (field.isAnnotationPresent(TimeManage.class)) {
                TimeManage annotation = field.getAnnotation(TimeManage.class);
                String fieldName = field.getName();
                String valueName = annotation.value();
                int value = 0;
                try {
                    field.setAccessible(true);
                    value = field.getInt(time);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                System.out.println(result(fieldName) + ": " + valueName + " " + value);
            }
        }
    }

    private static String result(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}