import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@interface TimeManage {
    String value() default "minute";
}
public class Time {
    @TimeManage("hour")
    private int hour;
    @TimeManage("minute")
    private int minute;
    @TimeManage("second")
    private int second;
    @TimeManage("nanosecond")
    private int nanosecond;

    public Time(int hour, int minute, int second, int nanosecond) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.nanosecond = nanosecond;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public int getNanosecond() {
        return nanosecond;
    }
}
