import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface OperationSystem {
    String value();
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface Phone {
    String value();
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface Technology {
    String value();
}
public class Tech {
    @OperationSystem("kaliLinux")
    private String os;

    @Phone("Samsung")
    private String phone;

    @Technology("AI")
    private String technology;

    public Tech(String os, String phone, String technology) {
        this.os = os;
        this.phone = phone;
        this.technology = technology;
    }

    public void printProductInfo() {
        Class<? extends Tech> techClass = this.getClass();
        for (java.lang.reflect.Field field : techClass.getDeclaredFields()) {
            if (field.isAnnotationPresent(OperationSystem.class) || field.isAnnotationPresent(Phone.class) ||
                    field.isAnnotationPresent(Technology.class)) {
                String fieldName = field.getName();
                try {
                    field.setAccessible(true);
                    String fieldValue = (String) field.get(this);
                    String annotationName = "";
                    if (field.isAnnotationPresent(OperationSystem.class)) {
                        OperationSystem annotation = field.getAnnotation(OperationSystem.class);
                        annotationName = annotation.value();
                    } else if (field.isAnnotationPresent(Phone.class)) {
                        Phone annotation = field.getAnnotation(Phone.class);
                        annotationName = annotation.value();
                    } else if (field.isAnnotationPresent(Technology.class)) {
                        Technology annotation = field.getAnnotation(Technology.class);
                        annotationName = annotation.value();
                    }
                    System.out.println(capitalize(fieldName) + ": " + annotationName + " " + fieldValue);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String capitalize(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}
