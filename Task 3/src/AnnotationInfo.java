public class AnnotationInfo {
    private String annotationName;

    public AnnotationInfo(String annotationName) {
        this.annotationName = annotationName;
    }

    public String getAnnotationName() {
        return annotationName;
    }

    @Override
    public String toString() {
        return "@" + annotationName;
    }
}
