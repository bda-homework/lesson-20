import java.util.ArrayList;
import java.util.List;
public class ConstructorInfo {
    private String name;
    private List<FieldInfo> parameters;

    public ConstructorInfo(String name) {
        this.name = name;
        this.parameters = new ArrayList<>();
    }

    public void addParameter(FieldInfo parameter) {
        parameters.add(parameter);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n\t{\n");
        sb.append("\t\tname: ").append(name).append(",\n");
        sb.append("\t\tparameters: [");
        if (!parameters.isEmpty()) {
            sb.append(parameters.get(0));
            for (int i = 1; i < parameters.size(); i++) {
                sb.append(", ").append(parameters.get(i));
            }
        }
        sb.append("]\n\t}");
        return sb.toString();
    }
}
