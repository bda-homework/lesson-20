import java.util.ArrayList;
import java.util.List;
public class FieldInfo {
    private String fieldName;
    private String fieldType;
    private List<AnnotationInfo> annotations;

    public FieldInfo(String fieldName, String fieldType) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.annotations = new ArrayList<>();
    }

    public void addAnnotation(AnnotationInfo annotation) {
        annotations.add(annotation);
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n\t{\n");
        sb.append("\t\tfieldName: ").append(fieldName).append(",\n");
        sb.append("\t\tfieldType: ").append(fieldType).append(",\n");
        sb.append("\t\tannotations: [");
        if (!annotations.isEmpty()) {
            sb.append(annotations.get(0));
            for (int i = 1; i < annotations.size(); i++) {
                sb.append(", ").append(annotations.get(i));
            }
        }
        sb.append("]\n\t}");
        return sb.toString();
    }
}
