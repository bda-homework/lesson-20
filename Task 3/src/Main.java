
public class Main {
    public static void main(String[] args) {
        Type userClass = new Type("model", "User");
        userClass.addField(new FieldInfo("name", "String"));
        userClass.addField(new FieldInfo("surname", "String"));
        userClass.addField(new FieldInfo("age", "Integer"));

        MethodInfo getNameMethod = new MethodInfo("getName", "String");
        getNameMethod.addAnnotation(new AnnotationInfo("Override"));
        MethodInfo setNameMethod = new MethodInfo("setName", "void");
        setNameMethod.addAnnotation(new AnnotationInfo("SuppressWarnings"));
        setNameMethod.addParameter(new FieldInfo("name", "String"));

        MethodInfo getSurnameMethod = new MethodInfo("getSurname", "String");
        MethodInfo getAgeMethod = new MethodInfo("getAge", "Integer");

        MethodInfo setSurnameMethod = new MethodInfo("setSurname", "void");
        setSurnameMethod.addParameter(new FieldInfo("surname", "String"));

        MethodInfo setAgeMethod = new MethodInfo("setAge", "void");
        setAgeMethod.addParameter(new FieldInfo("age", "Integer"));

        userClass.addMethod(getNameMethod);
        userClass.addMethod(setNameMethod);
        userClass.addMethod(getSurnameMethod);
        userClass.addMethod(getAgeMethod);
        userClass.addMethod(setSurnameMethod);
        userClass.addMethod(setAgeMethod);

        ConstructorInfo constructor1 = new ConstructorInfo("User");
        constructor1.addParameter(new FieldInfo("name", "String"));
        constructor1.addParameter(new FieldInfo("surname", "String"));
        constructor1.addParameter(new FieldInfo("age", "Integer"));

        ConstructorInfo constructor2 = new ConstructorInfo("User");

        userClass.addConstructor(constructor1);
        userClass.addConstructor(constructor2);

        System.out.println(userClass);
    }
}