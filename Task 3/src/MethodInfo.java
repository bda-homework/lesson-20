import java.util.ArrayList;
import java.util.List;
public class MethodInfo {
    private String methodName;
    private String returnType;
    private List<AnnotationInfo> annotations;
    private List<FieldInfo> parameters;

    public MethodInfo(String methodName, String returnType) {
        this.methodName = methodName;
        this.returnType = returnType;
        this.annotations = new ArrayList<>();
        this.parameters = new ArrayList<>();
    }

    public void addAnnotation(AnnotationInfo annotation) {
        annotations.add(annotation);
    }

    public void addParameter(FieldInfo parameter) {
        parameters.add(parameter);
    }

    public String getMethodName() {
        return methodName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n\t{\n");
        sb.append("\t\tmethodName: ").append(methodName).append(",\n");
        sb.append("\t\treturnType: ").append(returnType).append(",\n");
        sb.append("\t\tparameters: [");
        if (!parameters.isEmpty()) {
            sb.append(parameters.get(0));
            for (int i = 1; i < parameters.size(); i++) {
                sb.append(", ").append(parameters.get(i));
            }
        }
        sb.append("],\n");
        sb.append("\t\tannotations: [");
        if (!annotations.isEmpty()) {
            sb.append(annotations.get(0));
            for (int i = 1; i < annotations.size(); i++) {
                sb.append(", ").append(annotations.get(i));
            }
        }
        sb.append("]\n\t}");
        return sb.toString();
    }
}
