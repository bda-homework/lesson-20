import java.util.ArrayList;
import java.util.List;
public class Type {
    private String packageName;
    private String classType;
    private List<FieldInfo> fields;
    private List<MethodInfo> methods;
    private List<ConstructorInfo> constructors;

    public Type(String packageName, String classType) {
        this.packageName = packageName;
        this.classType = classType;
        this.fields = new ArrayList<>();
        this.methods = new ArrayList<>();
        this.constructors = new ArrayList<>();
    }

    public void addField(FieldInfo field) {
        fields.add(field);
    }

    public void addMethod(MethodInfo method) {
        methods.add(method);
    }

    public void addConstructor(ConstructorInfo constructor) {
        constructors.add(constructor);
    }

    public List<FieldInfo> getFields() {
        return fields;
    }

    public List<MethodInfo> getMethods() {
        return methods;
    }

    public List<ConstructorInfo> getConstructors() {
        return constructors;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Type: {\n");
        sb.append("\tpackageName: ").append(packageName).append(",\n");
        sb.append("\tclassType: ").append(classType).append(",\n");
        sb.append("\tfields: [");
        if (!fields.isEmpty()) {
            sb.append(fields.get(0));
            for (int i = 1; i < fields.size(); i++) {
                sb.append(",").append(fields.get(i));
            }
        }
        sb.append("],\n");
        sb.append("\tmethods: [");
        if (!methods.isEmpty()) {
            sb.append(methods.get(0));
            for (int i = 1; i < methods.size(); i++) {
                sb.append(",").append(methods.get(i));
            }
        }
        sb.append("],\n");
        sb.append("\tconstructors: [");
        if (!constructors.isEmpty()) {
            sb.append(constructors.get(0));
            for (int i = 1; i < constructors.size(); i++) {
                sb.append(",").append(constructors.get(i));
            }
        }
        sb.append("]\n}");
        return sb.toString();
    }
}
